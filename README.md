# EEvalidator

## Contexte du projet

Voir le cahier des charges en PDF.

Il y a volontairement peu d'information.

Pour les interface faites des WF. Pour le reste vous êtes libres.
Modalités pédagogiques

Travail individuel.
Modalités d'évaluation

Pour l'évaluation vous devez avoir une version en locale qui fonctionne. Il vous sera demander de réaliser un certain nombre d'opération pour vérifier le fonctionnement (créer des employé, des tickets, faire des validation etc.)
Livrables

Le code de l'application sur un repo git public, accompagné de la documentation produite.